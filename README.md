<p align="center">
  <img src="https://raw.githubusercontent.com/ihaolin/diablo/master/logo.jpg">
</p>

轻量的分布式配置管理平台
---

[English wiki](README_EN.md)。

<a href="http://diablo.hao0.me" target="_blank">在线Demo</a>: 用户名/密码(admin/admin123).

## 一些特性

+ **轻量级**: 没有复杂的技术和第三方依赖;

+ **可靠存储**: 使用**Redis**作为存储，建议使用**Redis Cluster**，**Redis Master-Slave** 或者 **Redis Proxy Middleware**等技术保证存储的可靠性;

+ **对等的Server节点**: Server节点的对等性，保证即便某些Server不可用，集群仍能工作;

+ **近似实时更新**: diablo使用**Http长轮询**来保证客户端能及时得到配置更新的通知;

+ **简单的UI**: diablo内嵌了一个简单易用的web界面，叫做**Diablo Tower**;

+ ...

## Diablo架构

![](diablo-arch.png)

+ 具体文档见<a href="https://github.com/ihaolin/diablo" target="_blank">这里</a>。